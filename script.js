document.addEventListener('DOMContentLoaded', function () {
    const parentElement = document.body;
    const data = ["Sun", "Moon", "Stars", ["Odessa", "Kyiv", "Kharkiv", "Lviv"]];

    function createListItems(data, parent) {
        const list = document.createElement('ul');
        parent.appendChild(list);

        data.map((item) => {
            const listItem = document.createElement('li');
            if (Array.isArray(item)) {
                createListItems(item, listItem);
            } else {
                listItem.textContent = item;
            }
            list.appendChild(listItem);
        });
    }

    createListItems(data, parentElement);

    // Set Timer
    let secondsLeft = 3;
    const countdownTimer = setInterval(function () {
        if (secondsLeft <= 0) {
            clearInterval(countdownTimer);
            parentElement.innerHTML = '';
        }
        secondsLeft--;
    }, 1000);
});
